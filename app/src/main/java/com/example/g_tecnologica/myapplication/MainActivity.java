package com.example.g_tecnologica.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private Button btn, btnSend;
    private ImageButton btnPhone, btnAntenna, btnSMS;
    private LocationManager locationManager;
    private boolean isGPSEnabled, isLoaddingGPS = false;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60; //   1 minute
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; //  10 metters
    private Location location;
    private double longitude, latitude;
    private JSONObject jsonObject;
    private  String server_url = "",
                    locationString = "",
                    m_text = "";
    private long volumeButtonPress = 0;
    private boolean isMsgSend = false;
    private SmsListener listener;   //  SMS

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.buttonMain);
        btnPhone = findViewById(R.id.imageButtonPhone);
        btnAntenna = findViewById((R.id.imageButtonAntenna));
        btnSMS = findViewById(R.id.imageButtonSMS);
        btnSend = findViewById(R.id.btnSend);
        listener = new SmsListener();
        //isNetWorkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        checkLocation();
        loadConfig();

        if (!isGPSEnabled){
            Toast.makeText(this, "Advertencia: No se ha habilitado el GPS en el dispositivo", Toast.LENGTH_SHORT).show();
        }

        if (!isNetWorkAvailable()){
            Toast.makeText(this, "Advertencia: No se ha detectado una red wifi conectada", Toast.LENGTH_SHORT).show();
        }

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentSMS = new Intent("android.provider.Telephony.SMS_RECEIVED");
                listener.onReceive(MainActivity.this, intentSMS);
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    checkLocation();
                    setLocation();
                    if (isNetWorkAvailable() && isGPSEnabled){
                        final RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
                        server_url = jsonObject.getString("server_url") + locationString + "/73966163";
                        //Toast.makeText(MainActivity.this, server_url, Toast.LENGTH_SHORT).show();
                        StringRequest stringRequest = new StringRequest(Request.Method.GET, server_url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        //editTextPhone.setText(response);
                                        Toast.makeText(MainActivity.this, "Alerta enviada exitosamente", Toast.LENGTH_LONG).show();
                                        requestQueue.stop();
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(MainActivity.this, "Ha ocurrido un error: " + error.getMessage(), Toast.LENGTH_LONG).show();
                                requestQueue.stop();
                            }
                        });
                        requestQueue.add(stringRequest);
                    }else if (!isNetWorkAvailable()){
                        Toast.makeText(MainActivity.this, "Advertencia: No se ha detectado una red wifi conectada", Toast.LENGTH_SHORT).show();
                    }else if(!isGPSEnabled){
                        Toast.makeText(MainActivity.this, "Advertencia: No se ha habilitado el GPS en el dispositivo", Toast.LENGTH_SHORT).show();
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

                    }else {


                        //  Creando SMS con ubicacion
                        SmsManager smsManager = SmsManager.getDefault();
                        JSONArray array = (JSONArray)jsonObject.get("sms_recipients");
                        for (int i = 0; i < array.length(); i++){
                            smsManager.sendTextMessage(array.getString(i),null, "Send help here please! http://maps.google.com/maps?q=" + latitude + "," + longitude, null, null);
                        }

                        //  Realizando llamada telefonica
                        OlderVersions(jsonObject.getString("phone"));
                    }
                }catch (Exception e){
                    Toast.makeText(MainActivity.this,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            private void OlderVersions(String phoneNumber) {
                Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                if (CheckPermissons(Manifest.permission.CALL_PHONE)) {
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                    startActivity(intentCall);
                }else{
                    Toast.makeText(MainActivity.this, "You declined the access", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Ingresa nuevo numero de telefono");

                    //  Setup the input
                    final EditText input = new EditText(MainActivity.this);
                    //  Specify the type of input expected
                    input.setInputType(InputType.TYPE_CLASS_PHONE);
                    input.setText(jsonObject.getString("phone"));
                    builder.setView(input);

                    //  Setup the buttons
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                m_text = input.getText().toString();
                                jsonObject.put("phone",m_text);
                                if (saveConfig()) Toast.makeText(MainActivity.this, "Numero guardado exitosamente", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                Toast.makeText(MainActivity.this,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

                    builder.show();
                }catch (Exception e){
                    Toast.makeText(MainActivity.this,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnAntenna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Ingresa nueva direccion a consultar");

                    //  Setup the input
                    final EditText input = new EditText(MainActivity.this);
                    //  Specify the type of input expected
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    input.setText(jsonObject.getString("server_url"));
                    builder.setView(input);

                    //  Setup the buttons
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                m_text = input.getText().toString();
                                jsonObject.put("server_url",m_text);
                                if (saveConfig()) Toast.makeText(MainActivity.this, "URL guardada exitosamente", Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                Toast.makeText(MainActivity.this,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    });

                    builder.show();
                }catch (Exception e){
                    Toast.makeText(MainActivity.this,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intentSMS = new Intent(MainActivity.this, ReadActivity.class);
                    intentSMS.putExtra("object", jsonObject.toString());
                    startActivityForResult(intentSMS,1);
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loadConfig(){
        try {
            String ret;
            InputStream stream = openFileInput("config");
            if (stream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(stream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String recieveString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((recieveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(recieveString);
                }

                stream.close();
                ret = stringBuilder.toString();

                jsonObject = new JSONObject(ret);
                JSONArray array = (JSONArray) jsonObject.get("sms_recipients");
            } else {
                Toast.makeText(this, "Archivo no leido", Toast.LENGTH_SHORT).show();
            }
        }catch (FileNotFoundException fnf){
            try{
                jsonObject = new JSONObject();
                jsonObject.put("server_url","http://google.com");
                jsonObject.put("phone", "72028728");
                JSONArray array = new JSONArray();
                array.put("72028728");
                array.put("75444024");
                jsonObject.put("sms_recipients", array);
                saveConfig();
            }catch (Exception e){
                Toast.makeText(this, "Error en creacion de archivo: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }

        }catch (Exception e){
            Toast.makeText(this, "Error en loadConfig: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean saveConfig(){
        try {
            FileOutputStream fos = openFileOutput("config", Context.MODE_PRIVATE);
            fos.write(jsonObject.toString().getBytes());
            fos.close();
            return true;
        }catch (Exception e){
            Toast.makeText(MainActivity.this,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void setLocation(){
        //  If GPS is Enabled get lat/long using GPS services
        if (isGPSEnabled){
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) return;
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
            Log.d("activity","RLOC: GPS Enabled");
            if (locationManager != null){
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null){
                    Log.d("activity", "RLOC: loc by GPS");
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    locationString = latitude + "/" + longitude;
                    locationString = locationString.replace('.',',');
                }else{
                    Log.d("activity", "location is null");
                }
            }else{
                Log.d("activity", "locationManager is null");
            }
        }
    }

    private boolean CheckPermissons(String permission){
        int result = this.checkCallingOrSelfPermission(permission);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadConfig();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(this, "GPS Desconectado", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(this, "GPS Cargando...", Toast.LENGTH_SHORT).show();
        isLoaddingGPS = true;
    }

    @Override
    public void onLocationChanged(Location location) {
        //Toast.makeText(this, "New location: " + location, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    public boolean isNetWorkAvailable(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        return info != null;
    }

    private void checkLocation(){
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

//    @Override
//    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
//            Toast.makeText(this, "Entra", Toast.LENGTH_LONG).show();
//        }
//        return super.onKeyLongPress(keyCode, event);
//    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
            if (volumeButtonPress == 0){
                volumeButtonPress = event.getEventTime();
            }else{
                long substract = event.getEventTime() - volumeButtonPress;
                if (substract >= 3000 && isMsgSend == false){
                    Toast.makeText(this, "Mensaje de SOS enviado", Toast.LENGTH_SHORT).show();
                    isMsgSend = true;
                }
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
            volumeButtonPress = 0;
            isMsgSend = false;
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}
