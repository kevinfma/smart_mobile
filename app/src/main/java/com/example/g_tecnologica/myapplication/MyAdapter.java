package com.example.g_tecnologica.myapplication;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.util.List;

public class MyAdapter extends BaseAdapter{

    private Context context;
    private int layout;
    private List<String> names;
    private JSONObject jsonObject;

    public MyAdapter(Context context, int layout, List<String> names, JSONObject jsonObject){
        this.context = context;
        this.layout = layout;
        this.names = names;
        this.jsonObject = jsonObject;
    }

    @Override
    public int getCount() {
        return this.names.size();
    }

    @Override
    public Object getItem(int position) {
        return this.names.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        View v = convertView;

        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        v = layoutInflater.inflate(R.layout.list_item,null);

        final String currentName = names.get(position);

        TextView textView = (TextView) v.findViewById(R.id.textView);
        ImageView imageView = (ImageView) v.findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("¿Deseas remover este elemento: " + currentName + "?");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try{
                                names.remove(position);
                                JSONArray array = new JSONArray(names);
                                jsonObject.put("sms_recipients", array);
                                if(ReadActivity.saveConfig(jsonObject,context)){
                                    Toast.makeText(context, "Se ha eliminado el numero: " + currentName, Toast.LENGTH_SHORT).show();
                                    Intent intentReturn = new Intent(context,MainActivity.class);
                                    intentReturn.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(intentReturn);
                                }else{
                                    Toast.makeText(context, "No de ha podido eliminar el numero " + currentName, Toast.LENGTH_SHORT).show();
                                }
                            }catch (Exception e){
                                Toast.makeText(context, "Error en click de aceptar: ", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    builder.show();
                }catch (Exception e){
                    Toast.makeText(context, "Error en creacion de builder: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        textView.setText(currentName);

        return v;
    }

    private boolean saveConfig(Context context){
        try {
            FileOutputStream fos = context.openFileOutput("config", Context.MODE_PRIVATE);
            fos.write(jsonObject.toString().getBytes());
            fos.close();
            return true;
        }catch (Exception e){
            Toast.makeText(context,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}
