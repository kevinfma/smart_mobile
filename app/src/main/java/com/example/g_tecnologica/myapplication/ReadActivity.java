package com.example.g_tecnologica.myapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ReadActivity extends AppCompatActivity {

    private ListView listView;
    private List<String> sms;
    private JSONArray jsonArray;
    private JSONObject jsonObject;
    private ImageButton imageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        listView = findViewById(R.id.listView);
        imageButton = findViewById(R.id.buttonAdd);

        Bundle bundle = getIntent().getExtras();
        sms = new ArrayList<String>();
        List<String> content = new ArrayList<String>();

        try {
            jsonObject = new JSONObject(bundle.getString("object"));
            jsonArray = (JSONArray) jsonObject.get("sms_recipients");
            for (int i = 0; i < jsonArray.length(); i++){
                sms.add(jsonArray.get(i).toString());
            }
        } catch (JSONException e) {
            Toast.makeText(this,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        //  Adaptador, la forma visual en la que mostraremos los datos
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,sms);

        //  Enlazamos el adaptador con nuestro listview
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int positiion, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ReadActivity.this);
                builder.setTitle("Ingresa nuevo numero");

                final EditText input = new EditText(ReadActivity.this);
                input.setInputType(InputType.TYPE_CLASS_PHONE);
                input.setText(sms.get(positiion));
                builder.setView(input);

                //  Setup the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try{
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                jsonArray.remove(positiion);
                            }
                            jsonArray.put(input.getText().toString());
                            jsonObject.put("sms_recipients", jsonArray);
                            if (saveConfig(jsonObject,ReadActivity.this)){
                                Toast.makeText(ReadActivity.this, "Se ha guardado el numero: " + input.getText().toString(),Toast.LENGTH_SHORT).show();
                                returnToParent();
                            }

                        }catch (Exception e){
                            Toast.makeText(ReadActivity.this,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.show();
            }
        });

        MyAdapter myAdapter = new MyAdapter(this, R.layout.list_item, sms, jsonObject);
        listView.setAdapter(myAdapter);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ReadActivity.this);
                builder.setTitle("Ingresa numero");
                final EditText input = new EditText(ReadActivity.this);
                input.setInputType(InputType.TYPE_CLASS_PHONE);

                builder.setView(input);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        jsonArray.put(input.getText().toString());
                        if (saveConfig(jsonObject,ReadActivity.this)){
                            Toast.makeText(ReadActivity.this, "Se ha registrado el numero: " + input.getText().toString(), Toast.LENGTH_SHORT).show();
                            returnToParent();
                        }else{
                            Toast.makeText(ReadActivity.this, "No se pudo registrar el numero", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                builder.show();
            }
        });
    }

    public static boolean saveConfig(JSONObject jsonObject, Context context){
        try {
            FileOutputStream fos = context.openFileOutput("config", Context.MODE_PRIVATE);
            fos.write(jsonObject.toString().getBytes());
            fos.close();
            return true;
        }catch (Exception e){
            Toast.makeText(context,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void iterateArray(){
        for (int i = 0; i < jsonArray.length(); i++){
            try {
                Toast.makeText(ReadActivity.this,jsonArray.get(i).toString(), Toast.LENGTH_SHORT).show();
            } catch (JSONException e) {
                Toast.makeText(this,"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void returnToParent(){
        Intent intentParent = new Intent(ReadActivity.this, MainActivity.class);
        intentParent.putExtra("object", jsonObject.toString());
        setResult(RESULT_OK, intentParent);
        finish();
    }
}

