package com.example.g_tecnologica.myapplication;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by G-Tecnologica on 4 may 2018.
 */

public class SmsListener extends BroadcastReceiver implements LocationListener{
    private LocationManager locationManager;
    private boolean isGPSEnabled;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60; //   1 minute
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; //  10 metters
    private Location location;
    private double longitude, latitude;
    private static final String SERVER_URL = "http://10.87.223.102:93/Test/Send/";
    private String locationString = "";

    @Override
    public void onReceive(final Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            checkLocation(context);
            setLocation(context);
            //Toast.makeText(context, "Location: " + locationString, Toast.LENGTH_SHORT).show();

            Bundle bundle =intent.getExtras();
            SmsMessage[] msgs = null;
            String msg_from;
            if (bundle != null){
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++){
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        msg_from = msgs[i].getOriginatingAddress().substring(4);
                        Toast.makeText(context, "" + msg_from, Toast.LENGTH_LONG).show();
                        String msgBody = msgs[i].getMessageBody();
                        String[] parts = msgBody.split("=");
                        String[] parts1 = parts[1].split(" ");
//                        Toast.makeText(context, ""+parts1[0], Toast.LENGTH_SHORT).show();
//                        Toast.makeText(context, "Body: " + msgBody, Toast.LENGTH_LONG).show();

                        parts1[0] = parts1[0].replace(',','/');
                        parts1[0] = parts1[0].replace('.',',');

                        final RequestQueue requestQueue = Volley.newRequestQueue(context);
                        String server_url = SERVER_URL + parts1[0] + "/" + msg_from;
//                        Toast.makeText(context, "" + server_url, Toast.LENGTH_LONG).show();
//                        Toast.makeText(context, SERVER_URL, Toast.LENGTH_SHORT).show();
                        StringRequest stringRequest = new StringRequest(Request.Method.GET, server_url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        //editTextPhone.setText(response);
                                        Toast.makeText(context, "Alerta enviada exitosamente", Toast.LENGTH_LONG).show();
                                        requestQueue.stop();
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(context, "Ha ocurrido un error: " + error.toString(), Toast.LENGTH_LONG).show();
                                requestQueue.stop();
                            }
                        });
                        requestQueue.add(stringRequest);
                    }
                }catch (Exception e){
                    Log.d("onReceive_error","Error: " + e.getMessage());
                }
            }
        }
    }

    private void setLocation(Context context) {
        //  If GPS is Enabled get lat/long using GPS services
        if (isGPSEnabled){
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) return;
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,MIN_TIME_BW_UPDATES,MIN_DISTANCE_CHANGE_FOR_UPDATES,this);
            Log.d("activity","RLOC: GPS Enabled");
            if (locationManager != null){
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null){
                    Log.d("activity", "RLOC: loc by GPS");
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    locationString = latitude + "/" + longitude;
                    locationString = locationString.replace('.',',');
                }else{
                    Log.d("activity", "location is null");
                }
            }else{
                Log.d("activity", "locationManager is null");
            }
        }
    }

    private void checkLocation(Context context){
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        assert locationManager != null;
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
